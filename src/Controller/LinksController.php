<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Links Controller
 *
 * @property \App\Model\Table\LinksTable $Links
 *
 * @method \App\Model\Entity\Link[] paginate($object = null, array $settings = [])
 */
class LinksController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Categorias');
        $categorias = $this->Categorias->find('list')
            ->where(['Categorias.situacao_id' => 1]);

        $query = $this->Links->find('all')
            ->contain(['Categorias', 'Situacoes'])
            ->where(['Links.cadastrado_por' => $this->Auth->user('id')])
            ->andWhere(['Links.situacao_id' => 1]);

        if(!empty($this->request->getQuery('titulo_filtro'))){
            $query->andWhere(['Links.titulo LIKE' => '%' .$this->request->getQuery('titulo_filtro') . '%']);
        }

        if(!empty($this->request->getQuery('categoria_id_filtro'))){
            $query->andWhere(['Links.categoria_id' => $this->request->getQuery('categoria_id_filtro')]);
        }

        $links = $this->paginate($query);

        $this->set(compact('links', 'categorias'));
        $this->set('_serialize', ['links']);
    }

    /**
     * View method
     *
     * @param string|null $id Link id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $link = $this->Links->get($id, [
            'contain' => ['Categorias', 'Situacoes', 'Users']
        ]);

        $this->set('link', $link);
        $this->set('_serialize', ['link']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $link = $this->Links->newEntity();
        if ($this->request->is('post')) {
            $link = $this->Links->patchEntity($link, $this->request->getData());
            $link->cadastrado_por = $this->Auth->user('id');
            $link->situacao_id = 1;

            if ($this->Links->save($link)) {
                $this->Flash->success(__('O Link foi salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O link não foi salvo, tente novamente!'));
        }
        $categorias = $this->Links->Categorias->find('list', ['limit' => 200])
            ->where(['Categorias.situacao_id' => 1]);
        $situacoes = $this->Links->Situacoes->find('list', ['limit' => 200]);
        $this->set(compact('link', 'categorias', 'situacoes'));
        $this->set('_serialize', ['link']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Link id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $link = $this->Links->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $link = $this->Links->patchEntity($link, $this->request->getData());
            $link->alterado_por = $this->Auth->user('id');
            if ($this->Links->save($link)) {
                $this->Flash->success(__('O link foi salvo com sucesso!'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O link não foi salvo, tente novamnete!'));
        }
        $categorias = $this->Links->Categorias->find('list', ['limit' => 200])
            ->where(['Categorias.situacao_id' => 1]);
        $situacoes = $this->Links->Situacoes->find('list', ['limit' => 200]);
        $this->set(compact('link', 'categorias', 'situacoes'));
        $this->set('_serialize', ['link']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Link id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $link = $this->Links->get($id);
        $link->alterado_por = $this->Auth->user('id');
        $link->situacao_id = 2;
        if ($this->Links->save($link)) {
            $this->Flash->success(__('O link foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('O link não foi deletado, tente novamente'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
