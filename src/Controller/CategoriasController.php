<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Categorias Controller
 *
 * @property \App\Model\Table\CategoriasTable $Categorias
 *
 * @method \App\Model\Entity\Categoria[] paginate($object = null, array $settings = [])
 */
class CategoriasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $query = $this->Categorias->find('all')
            ->contain(['Situacoes']);

        $categorias = $this->paginate($query);

        $this->set(compact('categorias'));
        $this->set('_serialize', ['categorias']);
    }

    /**
     * View method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $categoria = $this->Categorias->get($id, [
            'contain' => ['Situacoes', 'Links', 'UserReg', 'UserAlt']
        ]);

        $this->set('categoria', $categoria);
        $this->set('_serialize', ['categoria']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $categoria = $this->Categorias->newEntity();
        if ($this->request->is('post')) {
            $categoria = $this->Categorias->patchEntity($categoria, $this->request->getData());
            $categoria->cadastrado_por = $this->Auth->user('id');
            $categoria->situacao_id = 1;
            if ($this->Categorias->save($categoria)) {
                $this->Flash->success(__('A categoria foi salva com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A categoria não foi salva. Tente novamente!'));
        }
        $situacoes = $this->Categorias->Situacoes->find('list', ['limit' => 200]);
        $this->set(compact('categoria', 'situacoes'));
        $this->set('_serialize', ['categoria']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $categoria = $this->Categorias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $categoria = $this->Categorias->patchEntity($categoria, $this->request->getData());
            $categoria->alterado_por = $this->Auth->user('id');
            if ($this->Categorias->save($categoria)) {
                $this->Flash->success(__('A categoria foi salva com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A categoria não foi salva. Tente novamente!'));
        }
        $situacoes = $this->Categorias->Situacoes->find('list', ['limit' => 200]);
        $this->set(compact('categoria', 'situacoes'));
        $this->set('_serialize', ['categoria']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $categoria = $this->Categorias->get($id);
        $categoria->alterado_por = $this->Auth->user('id');
        $categoria->situacao_id = 2;
        if ($this->Categorias->save($categoria)) {
            $this->Flash->success(__('A categoria foi deletada.'));
        } else {
            $this->Flash->error(__('A categoria não foi deletada, tente novamente!'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
