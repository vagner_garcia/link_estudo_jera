<div class="loginContainer">
    <h1 class="loginTitulo">LPE</h1>
    <?= $this->Form->create() ?>
    <?= $this->Form->control('username', ['label' => 'Usuário', 'class' => 'form-control']) ?>
    <?= $this->Form->control('password', ['label' => 'Senha', 'class' => 'form-control']) ?>
    <?= $this->Form->button('Entrar', ['class' => 'btn btn-primary', 'form-control']) ?>
    <?= $this->Form->end() ?>
    <div style="margin-top: 10px">

        <?= $this->Html->link($this->Form->button('Cadastrar', ['class' => 'btn btn-primary']),
            array('action' => 'add'), array('escape' => false, 'title' => "Novo usuário?"));
        ?>
    </div>
</div>
