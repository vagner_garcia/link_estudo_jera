<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="users form large-12 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Novo Usuário') ?></legend>
        <?php
        echo $this->Form->control('name', ['label' => 'Nome']);
        echo $this->Form->control('username', ['label' => 'Usuário']);
        echo $this->Form->control('password', ['label' => 'Senha']);
        ?>
    </fieldset>
    <div class="espacoBotao">
        <?= $this->Form->button('Salvar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
