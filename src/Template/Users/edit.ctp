<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Deletar'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Tem certeza que quer deletar? # {0}?', $user->id)]
            )
            ?></li>
        <li><?= $this->Html->link(__('Listar Usuário'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Editar Usuário') ?></legend>
        <?php
        echo $this->Form->control('name', ['label' => 'Nome']);
        echo $this->Form->control('username', ['label' => 'Usuário']);
        echo $this->Form->control('password', ['label' => 'Senha']);
        ?>
    </fieldset>
    <div class="espacoBotao">
        <?= $this->Form->button(__('Salvar')) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
