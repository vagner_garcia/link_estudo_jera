<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Link $link
 */
?>
<div class="links view large-12 medium-8 columns content">
    <h3><?= h($link->titulo) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Código') ?></th>
            <td><?= $this->Number->format($link->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Título') ?></th>
            <td><?= h($link->titulo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('URL') ?></th>
            <td><?= h($link->url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Categoria') ?></th>
            <td><?= $link->has('categoria') ?$link->categoria->descricao : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Situação') ?></th>
            <td><?= $link->has('situaco') ? $link->situaco->descricao : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cadastrado Por') ?></th>
            <td><?= $link->has('user') ? $link->user->name : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dt. Criação') ?></th>
            <td><?= $link->created ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dt. Alteração') ?></th>
            <td><?= $link->modified ?></td>
        </tr>
    </table>
</div>
