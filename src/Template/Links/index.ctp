<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Link[]|\Cake\Collection\CollectionInterface $links
 */
?>
<div class="links index large-12 medium-8 columns content">
    <h3><?= __('Links') ?></h3>

    <div class="row">
        <div class="col-lg-12">
            <?= $this->Form->create('Links', ['type' => 'get']) ?>
            <div class="col-md-4" style="display: inline-block">
                <?= $this->Form->control('titulo_filtro', ['label' => 'Título']); ?>
            </div>

            <div class="col-md-4" style="display: inline-block">
                <?= $this->Form->control('categoria_id_filtro', ['options' => $categorias, 'empty' => 'Selecione', 'label' => 'Categoria']); ?>
            </div>

            <div class="col-md-4">
                <?= $this->Form->button('Pesquisar', ['class' => 'btn btn-primary espacoBotao']) ?>
            </div>

            <?= $this->Form->end() ?>
        </div>
    </div>
    <hr>
    <div class="right">
        <?= $this->Html->link($this->Form->button('Novo Link', ['class' => 'btn btn-secondary']),
            array('action' => 'add'), array('escape' => false, 'title' => "Cadastrar Link"));
        ?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('titulo') ?></th>
            <th scope="col"><?= $this->Paginator->sort('url') ?></th>
            <th scope="col"><?= $this->Paginator->sort('categoria_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('situacao_id', 'Situação') ?></th>
            <th scope="col" class="actions"><?= __('Ações') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($links as $link): ?>
            <tr>
                <td><?= h($link->titulo) ?></td>
                <td><a target="_blank" , href="<?= h($link->url) ?>"><?= h($link->url) ?></a></td>
                <td><?= $link->has('categoria') ? $link->categoria->descricao : '' ?></td>
                <td><?= $link->has('situaco') ? $link->situaco->descricao : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $link->id]) ?> |
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $link->id]) ?> |
                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $link->id], ['confirm' => __('Are you sure you want to delete # {0}?', $link->id)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeiro')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
            <?= $this->Paginator->last(__('último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registro(s) de {{count}}')]) ?></p>
    </div>
</div>
