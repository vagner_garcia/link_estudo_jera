<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Link $link
 */
?>
<div class="links form large-12 medium-8 columns content">
    <?= $this->Form->create($link) ?>
    <fieldset>
        <legend><?= __('Novo Link') ?></legend>
        <?php
        echo $this->Form->control('titulo', ['label' => 'Título']);
        echo $this->Form->control('url', ['label' => 'URL']);
        echo $this->Form->control('categoria_id', ['options' => $categorias]);
        ?>
    </fieldset>
    <div class="espacoBotao">
        <?= $this->Form->button('Salvar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
