<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Categoria $categoria
 */
?>
<div class="categorias form large-12 medium-8 columns content">
    <?= $this->Form->create($categoria) ?>
    <fieldset>
        <legend><?= __('Editar Categoria') ?></legend>
        <?php
        echo $this->Form->control('descricao', ['label' => 'Descrição']);
        ?>
    </fieldset>
    <div class="espacoBotao">
        <?= $this->Form->button('Salvar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
