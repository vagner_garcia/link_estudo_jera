<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Categoria $categoria
 */
?>
<div class="categorias view large-12 medium-8 columns content">
    <h3><?= h($categoria->descricao) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Código') ?></th>
            <td><?= $this->Number->format($categoria->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descrição') ?></th>
            <td><?= h($categoria->descricao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Situação') ?></th>
            <td><?= $categoria->has('situaco') ?$categoria->situaco->descricao : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cadastrado Por') ?></th>
            <td><?= $categoria->has('user_reg') ? $categoria->user_reg->name : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Alterado Por') ?></th>
            <td><?= $categoria->has('user_alt') ? $categoria->user_alt->name : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dt. Cadastro') ?></th>
            <td><?= h($categoria->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dt. Alteração') ?></th>
            <td><?= h($categoria->modified) ?></td>
        </tr>
    </table>
</div>
