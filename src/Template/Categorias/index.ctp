<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Categoria[]|\Cake\Collection\CollectionInterface $categorias
 */
?>
<div class="categorias index large-12 medium-8 columns content">
    <h3><?= __('Categorias') ?></h3>
    <div class="right">
        <?= $this->Html->link($this->Form->button('Nova Categoria', ['class' => 'btn btn-secondary']),
            array('action' => 'add'), array('escape' => false, 'title' => "Cadastrar Categoria"));
        ?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('descricao', 'Descrição') ?></th>
            <th scope="col"><?= $this->Paginator->sort('situacao_id', 'Situação') ?></th>
            <th scope="col" class="actions"><?= __('Ações') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($categorias as $categoria): ?>
            <tr>
                <td><?= h($categoria->descricao) ?></td>
                <td style="<?= $categoria->situacao_id == 1 ? 'color: green' : 'color: red' ?>">
                    <?= $categoria->has('situaco') ? $categoria->situaco->descricao : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $categoria->id]) ?> |
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $categoria->id]) ?> |
                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $categoria->id], ['confirm' => __('Tem certeza que deletar o registro {0}?', $categoria->descricao)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeiro')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
            <?= $this->Paginator->last(__('último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registro(s) de {{count}}')]) ?></p>
    </div>
</div>
